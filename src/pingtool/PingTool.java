/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingtool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Jorge
 */
public class PingTool {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        ArrayList<Ping> listToPing = null;
        BufferedReader br = new BufferedReader(new FileReader("configFile.txt"));
        listToPing = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            listToPing.add(new Ping(line));
        }

        String path = System.getProperty("user.dir") + "\\stats";
        File file = new File(path);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        
        try {

            for (int i = 0; i < listToPing.size(); i++) {
                listToPing.get(i).start();
            }

        } catch (Throwable t) {
            System.err.println("Process dying");
            t.printStackTrace();
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingtool;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import javax.imageio.ImageIO;


/**
 *
 * @author Kenny
 */

//this class should be ran in a thread after initialized (so run is called)
public class QueueLogger implements Runnable
{   
    private ConcurrentLinkedQueue<LogInstance> logQueue; //the queue of logs that we will be using
    //private ConcurrentLinkedQueue<ImageQueueInstance> imgQueue; //queue of images to write
    private SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //format used when making file name
    private SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS :"); //format used when writing inside of file
    private String folderPath = null; //folder path that will be set optionally by the constructor  (ex: C:\\Users\\Name\\Desktop\\ )
    
    public String getFolderPath()
    {
        return folderPath;
    }

    
    public QueueLogger(String folderPath)
    {
        logQueue = new ConcurrentLinkedQueue<>();
        this.folderPath = folderPath;
        File folder = new File(folderPath);
        if(!folder.exists())
        {
            folder.mkdir();
        }
        
        
    }
    
    public QueueLogger()
    {
        logQueue = new ConcurrentLinkedQueue<>();
    }
    
     public void createLogInstance(String msg, String fileName, String currentClass, String method, Level logLevel, Exception e) throws IOException
    {
        LogInstance toLog = new LogInstance(msg, fileName, currentClass, method, logLevel, e);
        logQueue.add(toLog);
    }

    public void createLogInstance(String msg, String fileName, String currentClass, String method, Level logLevel) throws IOException
    {
        LogInstance toLog = new LogInstance(msg, fileName, currentClass, method, logLevel);
        logQueue.add(toLog);
    }
    
    public void createLogInstance(String msg, String fileName, Level logLevel) throws IOException
    {
        LogInstance toLog = new LogInstance(msg, fileName, logLevel);
        logQueue.add(toLog);
    }
    
    public void createLogInstance(String msg, String fileName) throws IOException
    {
        LogInstance toLog = new LogInstance(msg, fileName);
        logQueue.add(toLog);
    }
    
   

   
    
    /*
    LogInstances should be created to log to a given file.
        Use the constructor that includes what you want, 
        it is added to the Queue that will be emptied automatically
    
    */
    private class LogInstance
    {
        private String msg;
        private String fileName;
        private String currentClass;
        private String method;
        private Date timeToLog;
        private Level logLevel;
        private Exception exceptionPassed;
        
        
         private LogInstance(String msg, String fileName, String currentClass, String method, Level logLevel, Exception e) throws IOException 
        {
            if(msg == null || fileName == null)
            {
                throw new IOException("Message and filename of LogInstance must not be null");
            }
            this.msg = msg;
            this.fileName = fileName;
            this.currentClass = currentClass;
            this.method = method;
            this.logLevel = logLevel;
            this.exceptionPassed = e;
            this.timeToLog = new Date();
        }
       
        private LogInstance(String msg, String fileName, String currentClass, String method, Level logLevel) throws IOException 
        {
            if(msg == null || fileName == null)
            {
                throw new IOException("Message and filename of LogInstance must not be null");
            }
            this.msg = msg;
            this.fileName = fileName;
            this.currentClass = currentClass;
            this.method = method;
            this.logLevel = logLevel;
            this.timeToLog = new Date();
        }
        
        private LogInstance(String msg, String fileName, Level logLevel) throws IOException
        {
            if(msg == null || fileName == null)
            {
                throw new IOException("Message and filename of LogInstance must not be null");
            }
            this.msg = msg;
            this.fileName = fileName;
            this.logLevel = logLevel;
            this.timeToLog = new Date();
        }
        
        private LogInstance(String msg, String fileName) throws IOException
        {
            if(msg == null || fileName == null)
            {
                throw new IOException("Message and filename of LogInstance must not be null");
            }
            this.msg = msg;
            this.fileName = fileName;
            this.timeToLog = new Date();
        }
        
        
        
        public String getMsg() {
            return msg;
        }

        public String getFileName() {
            return fileName;
        }

        public String getCurrentClass() {
            return currentClass;
        }

        public String getMethod() {
            return method;
        } 
        
        public Date getTimeToLog()
        {
            return timeToLog;
        }
        
        public Level getLogLevel()
        {
            return logLevel;
        }
        
        public Exception getException()
        {
            return exceptionPassed;
        }
                
    }
    
   
    
    
    
     @Override
    public void run() 
    {
        LogInstance next = null;
        while(true)
        {
            try
            {
                while(logQueue.size() > 0)
                {
                    next = logQueue.poll(); //get the next element of the queue
                    if(next != null) //it should not be null, as we only entered this while loop if size was  > 0
                    {
                        System.out.println(next.msg);
                        writeLogInstance(next);
                    }
                   
                    
                } 
            }
            catch(Exception e)
            {
                System.out.println("Unexpected failure unloading logQueue : " );
                e.printStackTrace();
            }
           
        }
        
    }
    
    private boolean writeLogInstance(LogInstance toWrite)
    {
       int numTries = 0;
       while(numTries < 3)
       {
           BufferedWriter bw = null; //start with a null buffered writer
           try
           {
               if(folderPath == null) //if we have no folderPath, use the currentDirectory
               {
                    bw = new BufferedWriter(new FileWriter(fileDateFormat.format(toWrite.getTimeToLog()) + "_" + toWrite.getFileName(), true));
               }
               else //otherwise use the folderPath that was specified to write to
               {
                    bw = new BufferedWriter(new FileWriter(folderPath + fileDateFormat.format(toWrite.getTimeToLog()) + "_" + toWrite.getFileName(), true));
               }
               if(toWrite.getLogLevel() == null) //if our log level is null, write the date and the message to the file
               {
                    bw.write(longDateFormat.format(toWrite.getTimeToLog()) + toWrite.getMsg()); //write the message
               }
               else //else if the level is not null
               {
                   if(toWrite.getCurrentClass() == null) //log only with level, not class
                   {
                        bw.write(longDateFormat.format(toWrite.getTimeToLog()) + toWrite.getLogLevel().toString() + " : " + toWrite.getMsg()); //write the message
                   }
                   else //log with class/method
                   {
                       if(toWrite.getException() == null) //log with level, class, but no exception
                       {
                            bw.write(longDateFormat.format(toWrite.getTimeToLog()) + toWrite.getLogLevel().toString() + " : Class (" + toWrite.getCurrentClass() +")  Method (" + toWrite.getMethod() + ")  : " + toWrite.getMsg()); //write the message
                       }
                       else //log with level, class/method, and exception
                       { 
                           bw.write(longDateFormat.format(toWrite.getTimeToLog()) + toWrite.getLogLevel().toString() + " : Class (" + toWrite.getCurrentClass() +")  Method (" + toWrite.getMethod() + ")  : " + toWrite.getMsg()); //write the message ExceptionUtils.getStackTrace(toWrite.getException());
                           bw.newLine();
                          // bw.write(ExceptionUtils.getStackTrace(toWrite.getException()));
                       }
                           
                   }
                   
               }
               
               bw.newLine(); //create a new line
               bw.flush(); //flush
               
               return true;
           }
           catch(Exception e)
           {
               numTries++;
               if(numTries >= 3)
               {
                    System.out.println("File write to (" + toWrite.getFileName() + " ) failed (" + numTries + "). Message that was to be written : " + toWrite.getMsg() );
                    e.printStackTrace();
               }
           }
           finally
           {
               if(bw != null)
               {
                   try
                   {
                        bw.close();
                   }
                   catch(Exception e)
                   {
                       System.out.println("Flush failed:" ); 
                       e.printStackTrace();
                   }
               }
           }
       }
       return false;
    }
    
   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingtool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jorge
 */
public class Ping extends Thread {

    private String ip;
    private String failed;
    private String succeded;
    private FileWriter writer;
    private PrintWriter pWriter;
    private QueueLogger qLog;
    private Thread logThread;
    private String pathFile;

    public Ping(String ip) throws IOException {
        this.ip = ip;

        // writer = new FileWriter(pathFile);
        qLog = new QueueLogger(System.getProperty("user.dir") + "\\stats\\");
        logThread = new Thread(qLog);
        logThread.start();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFailed() {
        return failed;
    }

    public void setFailed(String failed) {
        this.failed = failed;
    }

    public String getSucceded() {
        return succeded;
    }

    public void setSucceded(String succeded) {
        this.succeded = succeded;
    }

    public FileWriter getWriter() {
        return writer;
    }

    public void setWriter(FileWriter writer) {
        this.writer = writer;
    }

    public void run() {
        try {

            String s = null;
            pathFile = System.getProperty("user.dir") + "\\stats" + "\\" + ip + ".txt";
            try {

//            pWriter = new PrintWriter(new File(pathFile));
                Process p = Runtime.getRuntime().exec("ping " + ip + " -t");

                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                // read the output from the command
                // System.out.println("Here is the standard output of the command:\n");
                while ((s = stdInput.readLine()) != null) {

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    String toWrite = s + " Date:" + dateFormat.format(date);

                    qLog.createLogInstance(toWrite, ip+".txt");
                    // writer.write(s + " Date:" + dateFormat.format(date));
                    System.out.println(s + "  ip =>" + ip + " Date:" + dateFormat.format(date));

                }

            } catch (IOException ex) {

                ex.printStackTrace();
            }
        } catch (Throwable t) {
            System.err.println("Process dying");
            t.printStackTrace();
        }

    }

}
